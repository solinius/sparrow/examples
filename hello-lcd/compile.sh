#!/bin/bash

avr-gcc -g -Os -mmcu=atmega1284 -c main.cpp lcd.cpp

avr-gcc -g -mmcu=atmega1284 -o main.elf main.o lcd.o

avr-objcopy -j .text -j .data -O ihex main.elf main.hex

rm *.o *.elf