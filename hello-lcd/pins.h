#include <avr/io.h>

#define PIN_LCD_RS PB0
#define PIN_LCD_RW PB1
#define PIN_LCD_E PB2

#define PORT_LCD_DATA PORTC
#define PIN_LCD_DATA PINC
#define DDR_LCD_DATA DDRC

#define PIN_LCD_D0 PC0
#define PIN_LCD_D1 PC1
#define PIN_LCD_D2 PC2
#define PIN_LCD_D3 PC3