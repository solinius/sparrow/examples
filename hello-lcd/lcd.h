#ifndef AARDVARK_LCD_HAL_H
#define AARDVARK_LCD_HAL_H

#include <avr/io.h>
#include "pins.h"

#define LCD_COMMAND_REGISTER 0
#define LCD_DATA_REGISTER 1

uint8_t lcd_read(uint8_t target);
extern uint32_t total_loops;

void lcd_wait_for_not_busy_4_bit();
void lcd_wait_for_not_busy_8_bit();

uint8_t lcd_data_transpose(uint8_t data);

/**
 * Mask for 4 LCD data lines (not necessarily in ascending order)
 */
#define LCD_DATA_LINES_MASK     0b11000110;

/**
 * Opposite mask of 4 LCD data lines
 */
#define LCD_DATA_LINES_ANTIMASK 0b11110000;

void write_to_lcd_4_bit(uint8_t destination, uint8_t data);
void write_to_lcd_4_bit(uint8_t destination, uint8_t data, bool wait_for_not_busy);

void write_to_lcd_8_bit(uint8_t destination, uint8_t data);
void write_to_lcd_8_bit(uint8_t destination, uint8_t data, bool wait_for_not_busy);

void lcd_position_cursor(uint8_t column, uint8_t line);

void lcd_initialize_display();

void lcd_clear_display();

void lcd_shift_display_left();

void lcd_return_home();

void lcd_display_off();

void lcd_display_on();

void lcd_write_string(const char *string);

void lcd_initialize_pins();

void lcd_set_data_lines(uint8_t data);

uint8_t lcd_read_data_lines();

void lcd_data_lines_output();

void lcd_data_lines_input();

#endif //AARDVARK_LCD_HAL_H