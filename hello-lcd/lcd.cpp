#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include "lcd.h"
#include "pins.h"

uint8_t lcd_read(uint8_t target) {
    uint8_t data;

    if (target) {
        // aim at data register
        PORTB |= (1 << PIN_LCD_RS);
    } else {
        // aim at control register
        PORTB &= ~(1 << PIN_LCD_RS);
    }

    // read (R/W' high)
    PORTB |= (1<<PIN_LCD_RW);

    lcd_data_lines_input();

    // read high nibble
    PORTB |= (1 << PIN_LCD_E);
    _delay_us(1);
    data = PORTB << 4;
    //data |= (lcd_read_data_lines() << 4);
    PORTB &= ~(1 << PIN_LCD_E);

    _delay_us(1);

    // read low nibble
    PORTB |= (1 << PIN_LCD_E);
    _delay_us(1);
    data |= PORTB & 0x0F;
    //data |= lcd_read_data_lines();
    PORTB &= ~(1 << PIN_LCD_E);

    return data;
}

void lcd_wait_for_not_busy_4_bit() {
    register uint8_t status = lcd_read(LCD_COMMAND_REGISTER);
    uint16_t timeout = 1000;
    while (status & 0b10000000 && timeout--) {
        status = lcd_read(LCD_COMMAND_REGISTER);
    }
}

void lcd_wait_for_not_busy_8_bit() {
    // make data lines input
    DDRC = 0x00; // 0's to masked data bits

    // turn off pullups
    DDRC = 0x00;

    // aim at control register (RS low)
    PORTB &= ~(1<<PIN_LCD_RS);

    // read (R/W' high)
    PORTB |= (1<<PIN_LCD_RW);

    // enable data lines
    PORTB |= (1<<PIN_LCD_E);
    _delay_us(1);

    uint8_t busy_bit;
    // don't wait forever
    uint16_t timeout = 50000;
    do {
        // read data
        busy_bit = PINB & (1<<PB7);
    } while ((0x00 != busy_bit) && --timeout);

    // disable data lines
    PORTC &= ~(1<<PIN_LCD_E);

    // make data lines outputs again
    DDRC = 0xFF;

    if (0x00 == timeout) {
        // problem, busy bit never went low

    }
}

void write_to_lcd_4_bit_upper(uint8_t destination, uint8_t data, bool wait_for_not_busy) {
    if (wait_for_not_busy) {
        //lcd_wait_for_not_busy_4_bit();
        _delay_us(100);
    }

    // put high 4 bits on data lines
    uint8_t high_4 = (data & 0xF0);
    lcd_set_data_lines(high_4);

    // aim at the right register
    if (LCD_DATA_REGISTER == destination) {
        PORTB |= (1<<PIN_LCD_RS);
    } else {
        PORTB &= ~(1<<PIN_LCD_RS);
    }

    // we are writing (R/W' = low)
    PORTB &= ~(1<<PIN_LCD_RW);

    // wait for data lines to settle
    _delay_us(1);

    // clock data in
    PORTB |= 1<<PIN_LCD_E;
    _delay_us(1);
    PORTB &= ~(1<<PIN_LCD_E);
}

void write_to_lcd_4_bit(uint8_t destination, uint8_t data) {
    write_to_lcd_4_bit(destination, data, true);
}

void write_to_lcd_4_bit(uint8_t destination, uint8_t data, bool wait_for_not_busy) {
    if (wait_for_not_busy) {
        lcd_wait_for_not_busy_4_bit();
        //_delay_us(1000);
    }

    lcd_data_lines_output();

    // put high 4 bits on data lines
    uint8_t high_4 = (data & 0xF0) >> 4;
    lcd_set_data_lines(high_4);

    // aim at the right register
    if (LCD_DATA_REGISTER == destination) {
        PORTB |= (1<<PIN_LCD_RS);
    } else {
        PORTB &= ~(1<<PIN_LCD_RS);
    }

    // we are writing (R/W' = low)
    PORTB &= ~(1<<PIN_LCD_RW);

    // wait for data lines to settle
    _delay_us(1);

    // clock data in
    PORTB |= 1<<PIN_LCD_E;
    _delay_us(1);
    PORTB &= ~(1<<PIN_LCD_E);

    // put low 4 bits on data lines
    uint8_t low_4 = (data & 0x0F);
    lcd_set_data_lines(low_4);

    // wait for data lines to settle
    _delay_us(1);

    // clock data in
    PORTB |= 1<<PIN_LCD_E;
    _delay_us(1);
    PORTB &= ~(1<<PIN_LCD_E);
}

void write_to_lcd_8_bit(uint8_t destination, uint8_t data) {
    write_to_lcd_8_bit(destination, data, true);
}

void write_to_lcd_8_bit(uint8_t destination, uint8_t data, bool wait_for_not_busy) {
    if (wait_for_not_busy) {
        lcd_wait_for_not_busy_8_bit();
        //_delay_us(2000);
    }

    // put bits on data lines
    PORTB = data;

    // aim at the right register
    if (LCD_DATA_REGISTER == destination) {
        PORTB |= (1<<PIN_LCD_RS);
    } else {
        PORTB &= ~(1<<PIN_LCD_RS);
    }

    // we are writing (R/W' = low)
    PORTB &= ~(1<<PIN_LCD_RW);

    // wait for data lines to settle
    _delay_us(1);

    // clock data in
    PORTB |= 1<<PIN_LCD_E;
    _delay_us(1);
    PORTB &= ~(1<<PIN_LCD_E);
}

void lcd_position_cursor(uint8_t column, uint8_t line) {
    // set CGRAM
    // 1LCC CCCC
    // L = line, C = column
    uint8_t command = (1<<7) | (line ? 0x40 : 0x00) | (column & 0x3f);
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command);
}

void lcd_initialize_display() {
    // default control and data lines to something
    PORTB &= ~(1<<PIN_LCD_E);
    PORTB &= ~(1<<PIN_LCD_RS);
    PORTB &= ~(1<<PIN_LCD_RW);
    lcd_set_data_lines(0x00);

    _delay_ms(1000);

    uint8_t command;


    // "repeated procedures for 4-bit bus interface"
    for (int i = 0; i < 3; i++) {
        // 0000 XXXX
        write_to_lcd_4_bit_upper(LCD_COMMAND_REGISTER, 0x00, false);
        _delay_ms(200);
    }

    // blind write 8 bit command to put in 4 bit mode
    // 001L XXXX
    write_to_lcd_4_bit_upper(LCD_COMMAND_REGISTER, 0b00100000, false);
    _delay_ms(1000);

    // FUNCTION SET: 1 line, font size, font table
    // 001L NFFT
    // L = mode: 1=8-bit, 0=4-bit
    // N = lines: 1=2-line, 0=1-line
    // F = font: 1=5x10, 0=5x8
    // FT = font table: 00=English
    command = 0b00101000;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    _delay_ms(370);

    // CHARACTER MODE
    // 0001 GP11
    // G = mode: 1=graphic, 0=character
    // P = power: 1=on, 0=off
    command = 0b00010111;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    _delay_ms(370);

    // DISPLAY ON/OFF
    // 0000 1DCB
    // D = display on
    // C = cursor on
    // B = cursor blink
    command = 0b00001100;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    _delay_ms(370);

    // DISPLAY CLEAR
    // 0000 0001
    command = 0b00000001;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    _delay_ms(20);

    // ENTRY MODE SET
    // 0000 01IS
    // I = increment/decrement
    // S = scroll
    command = 0b00000110;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command, false);
    _delay_ms(20);
}

void lcd_clear_display() {
    // 0000 0001
    uint8_t command = 0b00000001;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command);
    _delay_us(1520);
}

void lcd_shift_display_left() {
    // 0001 DRxx
    // D = display/cusor: 1 = display, 0 = cursor
    // R = right/left; 1 = right, 0 = left
    uint8_t command = 0b00011000;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command);
}

void lcd_return_home() {
    // 0000 0010
    uint8_t command = 0b00000010;
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, command);
}

void lcd_display_off() {
    // DISPLAY ON/OFF
    // 0000 1DCB
    // D = display on
    // C = cursor on
    // B = cursor blink
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, 0b00001000, false);
}

void lcd_display_on() {
    // DISPLAY ON/OFF
    // 0000 1DCB
    // D = display on
    // C = cursor on
    // B = cursor blink
    write_to_lcd_4_bit(LCD_COMMAND_REGISTER, 0b00001100, false);
}

void lcd_write_string(const char *string) {
    lcd_return_home();
    _delay_ms(500);
    lcd_clear_display();
    _delay_ms(500);
    lcd_position_cursor(0, 0);
    _delay_us(1000);

    /*for (uint8_t i = 0; i < 16; i++) {
        write_to_lcd_4_bit(LCD_DATA_REGISTER, 65 + i);
    }*/

    uint8_t first_line_end = strlen(string) > 8 ? 8 : strlen(string);
    for (int i = 0; i < first_line_end; i++) {
        _delay_ms(5);
        write_to_lcd_4_bit(LCD_DATA_REGISTER, (uint8_t) string[i], false);
        //write_to_lcd_4_bit(LCD_DATA_REGISTER, 0x00);
    }

    if (strlen(string) > 8) {
        _delay_ms(500);
        lcd_position_cursor(0, 1);
        for (int i = 0; i < strlen(string) - first_line_end; i++) {
            _delay_ms(5);
            write_to_lcd_4_bit(LCD_DATA_REGISTER, (uint8_t) string[i + 8], false);
        }
    }
}

void lcd_initialize_pins() {
    // make control pins output
    DDRB |= (1<<PIN_LCD_RS) | (1<<PIN_LCD_RW) | (1<<PIN_LCD_E);

    // make data lines output
    lcd_data_lines_output();
}

/**
 * Turn the bottom 4 bits of data into an 8 bit number for PORT_LCD_DATA
 * @param data
 * @return
 */
uint8_t lcd_data_transpose(uint8_t data) {
    uint8_t bit0 = (data >> 0) & 0x01;
    uint8_t bit1 = (data >> 1) & 0x01;
    uint8_t bit2 = (data >> 2) & 0x01;
    uint8_t bit3 = (data >> 3) & 0x01;

    uint8_t out = (bit0 << PIN_LCD_D0) | (bit1 << PIN_LCD_D1) | (bit2 << PIN_LCD_D2) | (bit3 << PIN_LCD_D3);

    return out;
}

/**
 * Set the 4 LCD data lines with the bottom 4 bits of data.
 * @param data Value to put on data lines. Top 4 bits are ignored.
 */
void lcd_set_data_lines(uint8_t data) {
    // ignore top 4 bits
    data = data & 0x0F;

    // shift bits into their positions on the PORT
    uint8_t data_transposed = lcd_data_transpose(data);

    // grab the existing, non-lcd data on the port
    uint8_t non_lcd_data = PORT_LCD_DATA & LCD_DATA_LINES_ANTIMASK;

    // put both the existing and new lcd data on the PORT
    PORT_LCD_DATA = data_transposed | non_lcd_data;
}

/**
 * Read the 4 data lines are return the values in the bottom 4 bits of a \c uint8_t.
 * Assumes the data lines' DDRs are already in input mode.
 * @return
 */
uint8_t lcd_read_data_lines() {
    uint8_t data = 0x00;

    // read each bit
    uint8_t bit0 = ((1 << PIN_LCD_D0) & PIN_LCD_DATA);
    uint8_t bit1 = ((1 << PIN_LCD_D1) & PIN_LCD_DATA);
    uint8_t bit2 = ((1 << PIN_LCD_D2) & PIN_LCD_DATA);
    uint8_t bit3 = ((1 << PIN_LCD_D3) & PIN_LCD_DATA);

    // mash together
    data |= (bit0 << 0) | (bit1 << 1) | (bit2 << 2) | (bit3 << 3);

    return data;
}

void lcd_data_lines_output() {
    DDR_LCD_DATA |= (1 << PIN_LCD_D0);
    DDR_LCD_DATA |= (1 << PIN_LCD_D1);
    DDR_LCD_DATA |= (1 << PIN_LCD_D2);
    DDR_LCD_DATA |= (1 << PIN_LCD_D3);
}

void lcd_data_lines_input() {
    DDR_LCD_DATA &= ~(1 << PIN_LCD_D0);
    DDR_LCD_DATA &= ~(1 << PIN_LCD_D1);
    DDR_LCD_DATA &= ~(1 << PIN_LCD_D2);
    DDR_LCD_DATA &= ~(1 << PIN_LCD_D3);
}

