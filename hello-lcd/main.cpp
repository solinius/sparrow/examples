#define F_CPU 14745600

#include "main.h"
#include "lcd.h"
#include "util/delay.h"

int main() {
	setup();
	while (1) {
		loop();
	}
}

void setup() {
	// pwm for contrast
	init_pwm();

	// init lcd
	lcd_initialize_pins();
    lcd_initialize_display();
    _delay_ms(1000);
    lcd_initialize_display();

	// write to lcd
	lcd_write_string("twtr: @solinius");
}

void loop() {
	// do nothing

}

void init_pwm() {
    // WGM1[3:0] = 0b1111 (fast pwm, TOP = OCR1A)
    TCCR1A |= (1 << WGM11) | (1 << WGM10);
    TCCR1B |= (1 << WGM13) | (1 << WGM12);

    // COM1B[1:0] = 0b10 (clear OC1B pin on compare match, set OC1B pin at bottom)
    TCCR1A |= (1 << COM1B1);

    // CS1[2:0] = 0b011 (prescaler = clk/64)
    TCCR1B |= (1 << CS11) | (1 << CS10);

    // duty = flip/top ~= 89%
    OCR1A = 64;    // top: reset counter at this value
    OCR1B = 57;    // flip output (OC1B pin) at this counter value

    DDRD |= (1 << PD4);   // OC1B output
}